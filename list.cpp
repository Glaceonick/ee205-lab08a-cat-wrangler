///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Sort data
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/13/2021
///////////////////////////////////////////////////////////////////////////////



#include <iostream>
#include "list.hpp"
#include <cassert>

using namespace std;



/*
	public void push_front(Node* newNode) {
		if (head == NULL) {
			head = new Node(newNode);
			return;
		}
		Node current = head;
		while (current.next != NULL) {
			current = current.next;
		}
		current.next = new Node(newNode);
	}
*/

	const bool DoubleLinkedList::empty() const {
		if (head == nullptr && tail == nullptr) {
			return true;
		}
		return false;
	
	}

	
	void DoubleLinkedList::push_front(Node* newNode) {

		if (head == nullptr) {
			newNode->prev = nullptr;
			newNode->next = nullptr;
			tail = newNode;
			head = newNode;
			//cout << "New head/tail added... ";
		}

		else {
			newNode->prev = nullptr;
			newNode->next = head;
			head->prev = newNode;
			head = newNode;

			//cout << "Pushing to front... ";
		}

		//cout << "New size = " << size() << endl;

		return;
	}

/*
	public void deleteWithValue(int data) {
		if (head == NULL) return;
		if (head.data == data) {
			head = head.next;
			return;
		}

		Node current = head;
		while (current.next != NULL) {
			if (current.next.data == data) {
				current.next = current.next.next;
				return;
			}
			current = current.next;
		}
	}
*/

	Node* DoubleLinkedList::pop_front() {
		if (head == NULL) {
			return nullptr;
		}
		if (head == tail) {
		head = nullptr;
		tail = nullptr;
		return nullptr;
		}

		head = head->next;
		head->prev = nullptr;
		//cout << "Popping front... New Size = " << size() << endl;
		return 0;

	}

	Node* DoubleLinkedList::get_first() const {
		return head;
	}

	Node* DoubleLinkedList::get_next(const Node* currentNode) const {
		/*
		Node* current;
		current = currentNode;
		current = current->next;
		*/
		return currentNode->next;
	}

	unsigned int DoubleLinkedList::size() const {
		int i = 0;
		Node* current;
		current = head;
                while (current != NULL) {
                        current = current->next;
                	i++;
		}
		return i;

	}

	const bool DoubleLinkedList::validate() const {
                unsigned int count;
		count = size();
		//cout << "size of list is = " << size() << endl;

		if (head == nullptr) {
			assert( tail == nullptr);
			assert( count == 0);
			assert( empty() );
                }
		else {
			assert( tail != nullptr );
			assert( count != 0);
			assert( !empty() );
		}
		
		if (tail == nullptr) {
			assert( head == nullptr);
                        assert( count == 0);
                        assert( empty() );
                }
                else {
                        assert( head != nullptr );
                        assert( count != 0);
                        assert( !empty() );
                }

		if (head != nullptr && tail == head) {
			assert (count == 1);
		}

		unsigned int forwardCount = 0;
		Node* currentNode = head;

		while(currentNode != nullptr) {
			forwardCount++;
			if (currentNode->next != nullptr) {
				assert( currentNode->next->prev == currentNode);
			}
			currentNode = currentNode->next;
		}
		//cout << "forward count: " << forwardCount << endl;
		assert(count == forwardCount);

		unsigned int backwardCount = 0;
		currentNode = tail;
		while(currentNode != nullptr) {
			backwardCount++;
			if (currentNode->prev != nullptr) {
				assert( currentNode->prev->next == currentNode);
			}
			currentNode = currentNode->prev;
		}
		//cout << "backward count: " << backwardCount << endl << endl;
		assert( count == backwardCount);

		return true;

	}

	void DoubleLinkedList::push_back(Node* newNode) {
               
		if (newNode == nullptr)
			return;

		if (head == nullptr && tail == nullptr) {
			newNode->next = nullptr;
			newNode->prev = nullptr;
			head = newNode;
			tail = newNode;
			//cout << "it was empty" << endl;
			return;
		}
		if (head == tail) {
			newNode->prev = head;
			newNode->next = nullptr;
			head->next = newNode;
			tail = newNode;
			return;
		}

		newNode->prev = tail;
		newNode->next = nullptr;
		tail->next = newNode;
	        tail = newNode;
		return;
		
		
	
/*
	if (head == NULL) {
        tail->prev = NULL;
        head = newNode;
        return;
	}

	while (tail->next != NULL) {
        tail = tail->next;
	}

    	tail->next = newNode;

    	newNode->prev = tail;

    	return;
*/
    	}

	
	Node* DoubleLinkedList::get_last() const {
                return tail;
        }

	Node* DoubleLinkedList::pop_back() {
	
		if (head == nullptr && tail == nullptr) {
			//cout << "im already empty!" << endl;
			return nullptr;
		}

		if (head == tail) {
			head = nullptr;
			tail = nullptr;
			//cout << "popped the last one" << endl;
			return nullptr;
		}
		
		tail = tail->prev;
		tail->next = nullptr;
		return nullptr;		
 
	}

	Node* DoubleLinkedList::get_prev( const Node* currentNode ) const
		{
			return currentNode->prev;
		}

	void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode )
		{
			tempo = currentNode;
			if (currentNode == nullptr && head == nullptr) {
				push_front(newNode);
				return;
			}
			if (head == tail) {
				tail = newNode;
				tail->prev = head;
				head->next = tail;
				return;
			}
			if (newNode == nullptr) {
				 cout << "new node is invalid" << endl;
                                return;
                        }
			if (currentNode == tail) {
				push_back(newNode);
				return;
			}
			cout << "inserting after" << endl;
			tempo = currentNode->next;
			newNode->next = tempo;
			newNode->prev = currentNode;
			tempo->prev = newNode;
			currentNode->next = newNode;
			return;
		}

	void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode )
		{
			tempo = currentNode;
                        if (currentNode == nullptr && head == nullptr) {
                                push_front(newNode);
                                return;
                        }
                        if (head == tail) {
                                head = newNode;
                                head->next = tail;
                                tail->prev = head;
                                return;
                        }
                        if (newNode == nullptr) {
                                 cout << "new node is invalid" << endl;
                                return;
                        }
                        if (currentNode == head) {
                                push_front(newNode);
                                return;
                        }
                        cout << "inserting before" << endl;
                        tempo = currentNode->prev;
                        newNode->next = currentNode;
                        newNode->prev = tempo;
                        tempo->next = newNode;
                        currentNode->prev = newNode;
			return;
		}


	 const bool DoubleLinkedList::isIn(Node* check) { 
		 tempo = head;
		 while (tempo != nullptr) {
			 if (tempo == check) {
				 return true;
			 }
			 tempo = tempo->next;
		 }
		 return false;
	 }

	void DoubleLinkedList::swap ( Node* node1, Node* node2) {
		Node* left	= node1->prev;
		Node* right	= node1->next;
		Node* LEFT	= node2->prev;
		Node* RIGHT	= node2->next;

		if (node1 == node2) {
			cout << "Swapping the same" << endl;
			return;
		}

		if (node1 == nullptr || node2 == nullptr) {
			cout << "cannot swap with a nullptr" << endl;
			return;
		}
		
		///////////////////////////////////////
		if (size() ==2) {

		if (head->next == tail) {
			tempo = head;
			head = tail;
			tail = tempo;
			head->next = tail;
			tail->prev = head;
			head->prev = nullptr;
			tail->next = nullptr;

			cout << "swapped list of 2" << endl;
			return;

		}

		if (tail->next == head) {
                        tempo = tail;
                        tail = head;
                        head = tempo;
                        head->next = tail;
                        tail->prev = head;
                        head->prev = nullptr;
                        tail->next = nullptr;

                        cout << "swapped list of 2" << endl;
                        return;

                }
		}
		/////////////////////////////////////////

		
		tempo = node1;

		if (node1 == head && node2 == tail) {
                        cout << "head/tail swap" << endl;

                        head->next = nullptr;
                        head->prev = LEFT;
                        LEFT->next = head;
                        tail->prev = nullptr;
                        tail->next = right;
                        right->prev = tail;
			head = node2;
			tail = node1;
			return;
                }

                if (node2 == head && node1 == tail) {
                        cout << "tail/head swap" << endl;
			
			head->next = nullptr;
                        head->prev = left;
                        left->next = head;
                        tail->prev = nullptr;
                        tail->next = RIGHT;
                        RIGHT->prev = tail;
                        head = node2;
                        tail = node1;
			return;
                }

		//////////////////////////////////////////////////////////////////////

		if (node1 == head || node1 == tail || node2 == head || node2 == tail) {
			if (node1 == head) {
              			 if(node1->next == node2) {
                  			node1->next = RIGHT;
                			node1->prev = node2;
               				RIGHT->prev = node1;
                  			node2->next = node1;
                  			node2->prev = nullptr;
                  			head = node2;
					return;
               			} 
				 else {
                  			node1->next = RIGHT;
                  			node1->prev = LEFT;
                  			RIGHT->prev = node1;
                  			LEFT->next = node1;
                  			node2->next = right;
                  			node2->prev = nullptr;
                  			right->prev = node2;
                  			head = node2;
					return;
               				}
            		} 
			if (node1 == tail) {
               			if(node2->next == node1) {
                  			node1->prev = LEFT;
                  			node1->next = node2;
                  			LEFT->next = node1;
                  			node2->prev = node1;
                  			node2->next = nullptr;
                  			tail = node2;
					return;
               			} 
				else {
                  			node1->prev = LEFT;
                  			node1->next = RIGHT;
                  			LEFT->next = node1;
                  			RIGHT->prev = node1;
                  			node2->prev = left;
                  			node2->next = nullptr;
                  			left->next = node2;
                  			tail = node2;
					return;
               				}
            		} 
				
			if (node2 == head) {
              			if(node2->next == node1) {
                  			node2->next = right;
                  			node2->prev = node1;
                  			right->prev = node2;
                  			node1->next = node2;
                  			node1->prev = nullptr;
                  			head = node1;
					return;
               			} 
				else {
					node2->next = right;
					node2->prev = left;
					right->prev = node2;
					left->next = node2;
					node1->next = RIGHT;
					node1->prev = nullptr;
					RIGHT->prev = node1;
					head = node1;
					return;
        		       	}
            		} 
			if (node2 == tail) {
               			if(node1->next == node2) {
                  			node2->prev = left;
                  			node2->next = node1;
                  			left->next = node2;
                  			node1->prev = node2;
                  			node1->next = nullptr;
                  			tail = node1;
					return;		
               		} 
				else {
                  			node2->prev = left;
                  			node2->next = right;
                  			left->next = node2;
                  			right->prev = node2;
                  			node1->prev = LEFT;
                  			node1->next = nullptr;
                  			LEFT->next = node1;
                  			tail = node1;
					return;
 	     				}
            		}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		
				
				
		/*		
		cout << "regular swap" << endl;
		node1 = node2;
		node1->prev = left;
		node1->next = right;
		
		node2 = tempo;
		node2->prev = LEFT;
		node2->next = RIGHT;
		//////////////////////////////////////////////////
		}

		//head->prev = nullptr;
		//tail->next = nullptr;


		cout << "swapped" << endl;
		return;
*/
		int x = 0;
		if (node1->prev == head) {
			x=1;
		}
		if (node1->next == tail) {
			x=2;
		}
		if (node2->prev == head) {
                        x=3;
                }
		if (node2->next == tail) {
                        x=4;
                }

		cout << "x = " << x << endl;

		if (node1->next == node2){
                node1->next = RIGHT;
                node1->prev = node2;
                RIGHT->prev = node1;
                node2->next = node1;
                node2->prev = left;
                left->next = node2;
            	}

		else if (node2->next == node1){
                node2->next = right;
                node2->prev = node1;
                right->prev = node2;
                node1->next = node2;
                node1->prev = LEFT;
                LEFT->next = node1;
		}

		else {
                node1->next = RIGHT;
                node1->prev = LEFT;
                RIGHT->prev = node1;
                LEFT->next = node1;
                node2->next = right;
                node2->prev = left;
                left->next = node2;
                right->prev = node2;
            	}

		if (x == 1) {
			head->next = node2;
		}
		if (x == 2) {
			tail->prev = node2;
		}
		if (x == 3) {
			head->next = node1;
		}
		if (x == 4) {
			tail->prev = node1;
		}

	return;


	}

const bool DoubleLinkedList::isSorted() const{
	if (size() <= 1) {
		cout << "only one element" << endl;
		return true;
	}
	
	for (Node* i = head; i->next != nullptr; i = i->next) {
		if (*i > *i->next)
			return false;
	}
	return true;
}

void DoubleLinkedList::insertionSort() {
	if (isSorted()) {
		cout << "in order" << endl;
		return;
	}
	if (size() <= 1) {
		cout << " only one element" << endl;
		return;
	}


	Node* tempo1 = nullptr;
/*
	Node* tempo2 = nullptr;
	Node* tempo3 = nullptr;

	 while (tempo->next != nullptr) {
                tempo2 = tempo;
		tempo3 = tempo->next;
		while (tempo3 != nullptr) {
			if (tempo2 > tempo3) {
				tempo2 = tempo3;
			}
			tempo3 = tempo3->next;
		}
		swap(tempo, tempo2);
		tempo = tempo2;
                
                tempo = tempo->next;
	 }
*/

	for(Node* i = head; i->next != nullptr; i = i->next) {   
      		tempo1 = i;

      		for(Node* j = i->next; j != nullptr; j = j->next) {
         		if(*tempo1 > *j) {
            		tempo1 = j;
			}
      		}
      		swap(i, tempo1);
      		i = tempo1;
   }

	 return;
}





















