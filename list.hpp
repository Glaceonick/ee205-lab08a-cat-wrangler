///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Header file for list
///
/// Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/13/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

class DoubleLinkedList {
	public:
		const bool empty() const;
		void push_front( Node* newNode );
		Node* pop_front();
		Node* get_first() const;
		Node* get_next( const Node* currentNode ) const;
		unsigned int size() const;
		const bool validate() const;
		void push_back( Node* newNode );
                Node* pop_back();
                Node* get_last() const;
		Node* get_prev( const Node* currentNode ) const;
		void insert_after( Node* currentNode, Node* newNode );
		void insert_before( Node* currentNode, Node* newNode );
		const bool isIn(Node* check);
		void swap ( Node* node1, Node* node2);
		const bool isSorted() const;
		void insertionSort();
	protected:
		Node* head = nullptr;
		Node* tail = nullptr;
		Node* tempo = nullptr;

		unsigned int counter = 0;

	};



